package com.hua.androidibeaconlib;
/**
 * 存储一个iBeacon的基本信息
 * 附加说明：该类实现了Comparable<IBeacon>接口，能够其他排序API进行排序
 * @author liuzhenhua
 *
 */
public class IBeacon implements Comparable<IBeacon>{
	private String address;
	private String uuid;
	private int major;
	private int minor;
	private int rssi;
	private int tx_power;
	
	public IBeacon(String address,String uuid,int major,int minor,int rssi,int tx_power){
		this.address=address;
		this.uuid=uuid;
		this.major=major;
		this.minor=minor;
		this.rssi=rssi;
		this.tx_power=tx_power;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public int getMajor() {
		return major;
	}
	public void setMajor(int major) {
		this.major = major;
	}
	public int getMinor() {
		return minor;
	}
	public void setMinor(int minor) {
		this.minor = minor;
	}
	public int getRssi() {
		return rssi;
	}
	public void setRssi(int rssi) {
		this.rssi = rssi;
	}
	public int getTx_power() {
		return tx_power;
	}
	public void setTx_power(int tx_power) {
		this.tx_power = tx_power;
	}
	
	@Override
	public int compareTo(IBeacon other) {
		int uuid_compare_result=this.uuid.compareTo(other.uuid);
		if(uuid_compare_result!=0){
			return uuid_compare_result;
		}
		
		int major_compare_result=this.major-other.major;
		if(major_compare_result!=0){
			return major_compare_result;
		}
		
		int minor_compare_result=this.minor-other.minor;
		if(minor_compare_result !=0){
			return minor_compare_result;
		}
		
		//如果以上条件都不满足，则说明这两个iBeacon是同一个iBeacon，所以返回0
		return 0;
	}
	
	@Override
	public boolean equals(Object other) {
		//两个if条件都满足时才相等
		if(other instanceof IBeacon){
			IBeacon ibeacon=(IBeacon)other;
			if( compareTo(ibeacon)==0 ){
				return true;
			}
		}
		
		return false;
	}

	@Override
	public int hashCode() {
		return (this.address+this.uuid+this.major+this.minor+this.tx_power).hashCode();
	}

    @Override
    public String toString() {
        return this.address+" "+this.uuid+" "+this.major+" "+this.minor+" "+this.tx_power;
    }
}
