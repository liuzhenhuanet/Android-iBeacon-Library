package com.hua.androidibeaconlib;

import java.util.Collection;

/**
 * 收集到iBeacon的RSSI时的回调类
 * @author liuzhenhua
 *
 */
public interface IBeaconConsumer {
	public void onFoundIBeacon(Collection<IBeacon> ibeacons);
}
