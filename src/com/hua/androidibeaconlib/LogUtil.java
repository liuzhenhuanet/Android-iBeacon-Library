package com.hua.androidibeaconlib;

import android.util.Log;

/**
 * Created by Administrator on 2015/3/26 0026.
 */
public class LogUtil {
    private LogUtil(){  }

    /**
     * 在MainActivity中打印扫描到的iBeacon信息
     * @param message
     */
    public static void printMainActivity(String message){
        Log.e(MainActivity.class.getSimpleName(), "@"+message);
    }

    public static void printLE(String message){
        Log.e("LE", "$"+message);
    }

    /**
     * 测试空或者取消等操作
     * @param message
     */
    public static void printNull(String message){
        Log.e("null", "null "+message);
    }

}
