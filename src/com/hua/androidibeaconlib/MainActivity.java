package com.hua.androidibeaconlib;

import android.app.Activity;
import android.os.Bundle;

import java.util.Collection;

/**
 * Created by liuzhenhua on 2015/3/26 0026.
 * 用来测试的activity
 */
public class MainActivity extends Activity {
    IBeaconManager manager;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        manager = IBeaconManager.getInstanceForApplication(this);
        manager.bind(consumer);
    }

    @Override
    protected void onDestroy() {
        manager.unBind(consumer);
        super.onDestroy();
    }

    IBeaconConsumer consumer = new IBeaconConsumer() {
        @Override
        public void onFoundIBeacon(Collection<IBeacon> ibeacons) {
            for (IBeacon iBeacon : ibeacons) {
                LogUtil.printMainActivity(ibeacons.toString());
            }
        }
    };
}